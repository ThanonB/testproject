*** Settings ***
Library    Selenium2Library
Test Setup    open web google
Test Teardown    Close All Browsers


# *** Variables ***

*** Test Cases ***
Case 1
    [Tags]  Case1
    GIVEN set directory    Case1
    WHEN input data
    AND click button search
    THEN verified data success
Case 2
    [Tags]  Case2
    GIVEN set directory    Case2
    WHEN input data
    AND click button search
    THEN verified data fail

*** Keywords ***
set directory
    [Arguments]    ${CASE}
    Set Screenshot Directory   W:/# Automate Test/robotTestJenkins/results/${CASE}
open web google
    Open Browser    https://www.google.co.th    GC
input data
    Wait Until Page Contains Element    xpath://html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input    timeout=15seconds
    Input Text    xpath://html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input    robotframework
    # Click Element    xpath://html/body
    Click Element    xpath:/html/body/div[1]/div[3]/form/div[1]
    Capture Page Screenshot    step-{index}.png
    ## Press Keys    xpath://html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input    ENTER
click button search
    Wait Until Page Contains Element    xpath://html/body/div[1]/div[3]/form/div[1]/div[1]/div[3]/center/input[1]    timeout=15seconds
    Click Element    xpath://html/body/div[1]/div[3]/form/div[1]/div[1]/div[3]/center/input[1]
verified data success
    Page Should Contain    robotframework
    Capture Page Screenshot    step-{index}.png
verified data fail
    Page Should Not Contain    xxx
    Capture Page Screenshot    step-{index}.png
